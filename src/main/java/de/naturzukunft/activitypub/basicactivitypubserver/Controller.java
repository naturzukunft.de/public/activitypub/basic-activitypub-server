package de.naturzukunft.activitypub.basicactivitypubserver;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.tomitribe.auth.signatures.Algorithm;
import org.tomitribe.auth.signatures.Signature;
import org.tomitribe.auth.signatures.Signer;
import org.tomitribe.auth.signatures.SigningAlgorithm;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@org.springframework.stereotype.Controller
@Slf4j
public class Controller {

//	private final String host = "mastodon.social";
//	private final String uri = "/inbox";

	
	private String host = "friends.grishka.me";
	private final String uri = "/activitypub/sharedInbox"; //	https://friends.grishka.me/activitypub/sharedInbox
	
	private String keyId = "https://naturzukunft.ddnss.de/actor#main-key";
	private static String privateKeyString;
	private static PrivateKey privateKey;
	private static String publicKey;
	static {
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			KeyPair kp = kpg.generateKeyPair();
			
			privateKey = kp.getPrivate();
			
			Base64.Encoder encoder = Base64.getEncoder();
			StringBuilder sb = new StringBuilder();
			sb.append("-----BEGIN RSA PRIVATE KEY-----\n");
			sb.append(encoder.encodeToString(kp.getPrivate().getEncoded()));
			sb.append("\n-----END RSA PRIVATE KEY-----\n");
			privateKeyString = sb.toString();
			
			sb = new StringBuilder();
			sb.append("-----BEGIN RSA PUBLIC KEY-----\n");
			sb.append(encoder.encodeToString(kp.getPublic().getEncoded()));
			sb.append("\n-----END RSA PUBLIC KEY-----\n");
			publicKey = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}	
	}

	public Mono<ServerResponse> webfinger(ServerRequest request) {
		return ServerResponse.ok().body(BodyInserters.fromValue("{\n"
				+ "	\"subject\": \"acct:actor@naturzukunft.ddnss.de\",\n" 
				+ "\n" 
				+ "	\"links\": [\n" 
				+ "		{\n"
				+ "			\"rel\": \"self\",\n" 
				+ "			\"type\": \"application/activity+json\",\n"
				+ "			\"href\": \"https://naturzukunft.ddnss.de/actor\"\n" 
				+ "		}\n" 
				+ "	]\n" 
				+ "}"));
	}

	public Mono<ServerResponse> actor(ServerRequest request) {
		return ServerResponse.ok().body(BodyInserters.fromValue("{\n" + 
				"	\"@context\": [\n" + 
				"		\"https://www.w3.org/ns/activitystreams\",\n" + 
				"		\"https://w3id.org/security/v1\"\n" + 
				"	],\n" + "\n" + 
				"	\"id\": \"https://naturzukunft.ddnss.de/actor\",\n" + 
				"	\"type\": \"Person\",\n" + 
				"	\"preferredUsername\": \"alice\",\n" + 
				"	\"inbox\": \"https://naturzukunft.ddnss.de/actor/inbox\",\n" + 
				"\n" + 
				"	\"publicKey\": {\n" +
				"		\"id\": \"https://naturzukunft.ddnss.de/actor#main-key\",\n" + 
				"		\"owner\": \"https://naturzukunft.ddnss.de/actor\",\n" + 
				"		\"publicKeyPem\": \""
				+ getPublicKey().replace("\n", "\\n")  +"\"" + 
				"	},\n" +
				"	\"mainKey\": \""+ getPublicKey().replace("\n", "\\n")  +"\"" +
				"}"));
	}
	
	public Mono<ServerResponse> createHelloWorldActivity(ServerRequest request) {
		String msg = getRessource("message.json");
		return ServerResponse.ok().body(BodyInserters.fromValue(msg));
	}
	
	public Mono<ServerResponse> helloWorldObject(ServerRequest request) {
		String msg = getRessource("object.json");
		return ServerResponse.ok().body(BodyInserters.fromValue(msg));
	}
		
	public Mono<ServerResponse> key(ServerRequest request) {
		System.out.println("->key");
		String publicKey2 = getPublicKey();
		return ServerResponse.ok().body(BodyInserters.fromValue(publicKey2));
	}

	public Mono<ServerResponse> home(ServerRequest request) {
		return ServerResponse.ok().body(BodyInserters.fromValue("home"));
	}

	public Mono<ServerResponse> send(ServerRequest request) {
		String msg = getRessource("message.json");
		// send
		return httpPost(msg).flatMap(res -> ServerResponse.ok().body(BodyInserters.fromValue(res)));
	}

	private String getRessource(String name) {
		try {
			ClassPathResource cpr = new ClassPathResource(name);
			return IOUtils.toString(cpr.getInputStream(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String getPrivateKey() {
		return privateKeyString;
	}
	
	private String getPublicKey() {
		return publicKey;
	}

	private Mono<String> httpPost(String modelAsString) {
		return httpPostSmithereen(modelAsString);
//		return httpPostFredy(modelAsString);
	}
	
	private Mono<String> httpPostSmithereen(String modelAsString) {
		log.trace("modelAsString: " + modelAsString);
		String url = "https://" + host + uri;
		try {
			Smithereen.postActivity(new URI(url), modelAsString, privateKey, keyId);
		} catch (IOException | URISyntaxException e) {
			throw new RuntimeException(e);
		}
		return Mono.just("no response");
	}
	
	private Mono<String> httpPostFredy(String modelAsString) {
		log.trace("modelAsString: " + modelAsString);

		final String method = "post";

		try {
			String url = "https://" + host + uri;
		
			Map<String, String> headersMap = sign(modelAsString, getPrivateKey(), keyId, host, method);
			
			WebClient.Builder wcb = WebClient.builder()
					.filters(exchangeFilterFunctions -> {
						exchangeFilterFunctions.add(logRequest());
						exchangeFilterFunctions.add(logResponse());
						})
					.defaultHeaders(httpHeaders -> headersMap.forEach((k,v)-> httpHeaders.set(k, v)));
			
			return wcb.build().post().uri(url).body(BodyInserters.fromValue(modelAsString))
					.retrieve()
					.onStatus(HttpStatus::isError, this::createException)
					.toEntity(String.class)
					.doOnNext(it -> log.trace("post response: " + it))
					.flatMap(res -> {
						log.trace("Response: " + res.getStatusCode() + " - " + res.getBody());
						return getLocationHeader(res.getHeaders().get("Location"));
					})
					.doOnNext(location -> log.trace("post location: " + location));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * @param requestBody The body to send via http request
	 * @param privateKey the privateKey of a generated keyPair
	 * @param keyId the url, where the publicKey is accessable
	 * @param host The receiver of the http request
	 * @param httpMethod the http method which is used to sedn the http request
	 * @return map of header attributes to use in the http header
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException 
	 */
	private Map<String, String> sign(String requestBody, String privateKey, String keyId, String host, final String httpMethod)
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {

		final Map<String, String> headers = new LinkedHashMap<>();		
		headers.put("(request-target)", httpMethod + " " + uri);
		headers.put("host", host);
		headers.put("date", getCurrentHttpDate());
//		headers.put("digest", getDigest(requestBody));		
		
		
		List<String> headerKeys = new ArrayList<String>(headers.keySet());
//		headerKeys.add("(request-target)");
		
		SigningAlgorithm signingAlgorithm = SigningAlgorithm.RSA_SHA256;
		Algorithm algorithm = Algorithm.RSA_SHA256;
		
		final org.tomitribe.auth.signatures.Signature signature = new org.tomitribe.auth.signatures.Signature(
				keyId, signingAlgorithm, algorithm, null, null, headerKeys );		
		final Signer signer = new Signer(Controller.privateKey, signature);
		Signature signed = signer.sign(httpMethod, uri, headers);
		
		headers.put("signature", signed.toString().replace("Signature ", ""));
		
		signed.getHeaders().forEach(System.out::println);
		headers.entrySet().forEach(System.out::println);
		
		
		
//		headers.put("Authorization", signed.toString());
		return headers;
	}

	private String getDigest(String requestBody) throws NoSuchAlgorithmException {
		byte[] digest = MessageDigest.getInstance("SHA-256").digest(requestBody.getBytes());
		final String digestHeader = "SHA-256=" + new String(org.tomitribe.auth.signatures.Base64.encodeBase64(digest));
		return digestHeader;
	}

	private String getCurrentHttpDate() {
		return DateTimeFormatter.RFC_1123_DATE_TIME.withZone(ZoneOffset.UTC).format(Instant.now());
	}

	private Mono<WebClientResponseException> createException(ClientResponse clientResponse) {
		Mono<WebClientResponseException> ex = clientResponse.createException();
		log.error("HTTP ERROR !!");
		Mono<ResponseEntity<String>> entity = clientResponse.toEntity(String.class);
		log.error("body: " + entity.subscribe(System.out::println));
		return ex;
	}

	private Mono<String> getLocationHeader(List<String> locs) {
		if (locs == null) {
			return Mono.just("");
		}
		return Mono.just(locs).map(List::stream).map(Stream::findFirst).map(optional -> optional.orElse(""));
	}

	ExchangeFilterFunction logRequest() {
		return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
			if (log.isDebugEnabled()) {
				log.debug("Request: ");
				clientRequest.headers().forEach((name, values) -> values
						.forEach(value -> log.debug(name + " - " + values)/* append header key/value */));
			}
			return Mono.just(clientRequest);
		});
	}

	ExchangeFilterFunction logResponse() {
		return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
			if (log.isDebugEnabled()) {
				log.debug("Response: ");
				clientResponse.headers().asHttpHeaders().forEach((name, values) -> values.forEach(
						value -> log.debug(" header:" + name + " - value: " + values)/* append header key/value */));
			}
			return Mono.just(clientResponse);
		});
	}
}
