package de.naturzukunft.activitypub.basicactivitypubserver;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class Smithereen {

	private static OkHttpClient httpClient;
	
	static{
		httpClient=new OkHttpClient.Builder()
//				.addNetworkInterceptor(new DisallowLocalhostInterceptor())
//				.addNetworkInterceptor(new LoggingInterceptor())
				.build();
	}
	
	public static void postActivity(URI inboxUrl, String activityJson, PrivateKey privateKey, String keyID) throws IOException{
		if(privateKey==null)
			throw new IllegalArgumentException("Sending an activity requires an actor that has a private key on this server.");

		System.out.println("->postActivity ");
		System.out.println("inboxUrl: " + inboxUrl);
		System.out.println("activityJson: " + activityJson);
		System.out.println("keyID: " + keyID);
		System.out.println("privateKey: " + privateKey );
				
		byte[] body=activityJson.getBytes(StandardCharsets.UTF_8);
		Request req=signRequest(
					new Request.Builder()
					.url(inboxUrl.toString())
					.post(RequestBody.create(MediaType.parse("application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""), body)),
				inboxUrl, privateKey, keyID, body)
				.build();
		Response resp=httpClient.newCall(req).execute();
		System.out.println(resp.toString());
		try(ResponseBody rb=resp.body()){
			if(!resp.isSuccessful())
				System.out.println(rb.string());
		}
	}
	
	private static Request.Builder signRequest(Request.Builder builder, URI url, PrivateKey privateKey, String keyID, byte[] body){
		String path=url.getPath();
		String host=url.getHost();
		if(url.getPort()!=-1)
			host+=":"+url.getPort();
		SimpleDateFormat dateFormat=new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		String date=dateFormat.format(new Date());
		String digestHeader="SHA-256=";
		try{
			digestHeader+=Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(body));
		}catch(NoSuchAlgorithmException ignore){}
		String strToSign="(request-target): post "+path+"\nhost: "+host+"\ndate: "+date+"\ndigest: "+digestHeader;

		Signature sig;
		byte[] signature;
		try{
			sig=Signature.getInstance("SHA256withRSA");
			sig.initSign(privateKey);
			sig.update(strToSign.getBytes(StandardCharsets.UTF_8));
			signature=sig.sign();
		}catch(Exception x){
			x.printStackTrace();
			throw new RuntimeException(x);
		}

//		String keyID=actor.activityPubID+"#main-key";
		String sigHeader="keyId=\""+keyID+"\",headers=\"(request-target) host date digest\",algorithm=\"rsa-sha256\",signature=\""+Base64.getEncoder().encodeToString(signature)+"\"";
		builder.header("Signature", sigHeader).header("Date", date).header("Digest", digestHeader);
		return builder;
	}
}
