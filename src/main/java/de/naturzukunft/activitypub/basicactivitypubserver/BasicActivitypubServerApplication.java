package de.naturzukunft.activitypub.basicactivitypubserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.server.SecurityWebFilterChain;

@SpringBootApplication
@EnableWebFluxSecurity
public class BasicActivitypubServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicActivitypubServerApplication.class, args);
	}
	
    @Bean
    public MapReactiveUserDetailsService userDetailsService() {
        UserDetails user = User.withDefaultPasswordEncoder()
            .username("user")
            .password("user")
            .roles("USER")
            .build();
        return new MapReactiveUserDetailsService(user);
    }
    
    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http
            .authorizeExchange()
            	.pathMatchers(HttpMethod.GET, "/actor").permitAll()
            	.pathMatchers(HttpMethod.GET, "/key").permitAll()
            	.pathMatchers(HttpMethod.GET, "/hello-world").permitAll()
            	.pathMatchers(HttpMethod.GET, "/create-hello-world").permitAll()
                .anyExchange().authenticated()
                .and()
            .httpBasic().and()
            .formLogin();
        return http.build();
    }    
    
//	   @Bean
//	   public ReactiveSpringLoggingFilter reactiveSpringLoggingFilter() {
//	      return new ReactiveSpringLoggingFilter("", true, true);
//	   }
    
}
